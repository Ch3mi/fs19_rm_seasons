----------------------------------------------------------------------------------------------------
-- SeasonsMeasurementRequestEvent
----------------------------------------------------------------------------------------------------
-- Purpose:  Event to ask for measurement and to receive new data.
--           Only contains extra data
--
-- Copyright (c) Realismus Modding, 2019
----------------------------------------------------------------------------------------------------

SeasonsMeasurementRequestEvent = {}
local SeasonsMeasurementRequestEvent_mt = Class(SeasonsMeasurementRequestEvent, Event)

InitEventClass(SeasonsMeasurementRequestEvent, "SeasonsMeasurementRequestEvent")

function SeasonsMeasurementRequestEvent:emptyNew()
    local self = Event:new(SeasonsMeasurementRequestEvent_mt)
    return self
end

function SeasonsMeasurementRequestEvent:new(measuredObject)
    local self = SeasonsMeasurementRequestEvent:emptyNew()

    self.measuredObject = measuredObject

    return self
end

function SeasonsMeasurementRequestEvent:writeStream(streamId, connection)
    NetworkUtil.writeNodeObject(streamId, self.measuredObject)
end

function SeasonsMeasurementRequestEvent:readStream(streamId, connection)
    self.measuredObject = NetworkUtil.readNodeObject(streamId)

    self:run(connection)
end

function SeasonsMeasurementRequestEvent:run(connection)
    if not connection:getIsServer() and self.measuredObject ~= nil then
        MeasureTool.onRequestExtraMeasureData(connection, self.measuredObject)
    end
end
